/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef _MPFP_H_
#define _MPFP_H_

#include <gmp.h>

typedef struct _mpfp_t {
	mpz_t value;  /* value is stored as arbitrary size integer */
	int nbits;    /* number of fractional bits */
} mpfp_t[1], *mpfp_ptr;

/* Some function are direct pass-throughs to GMP */
#define mpfp_add(rop, a, b) mpz_add((rop)->value, (a)->value, (b)->value)
#define mpfp_sub(rop, a, b) mpz_sub((rop)->value, (a)->value, (b)->value)
#define mpfp_abs(rop, x) mpz_abs((rop)->value, (x)->value)
#define mpfp_neg(rop, x) mpz_neg((rop)->value, (x)->value)
#define mpfp_sgn(x) mpz_sgn((x)->value)
#define mpfp_cmp(a, b) mpz_cmp((a)->value, (b)->value)

/* Initialization / Cleanup */
void mpfp_init(mpfp_ptr, int);
void mpfp_inits(int, ...);
/*void mpfp_clear(mpfp_ptr);*/
#define mpfp_clear(x) mpz_clear((x)->value)
void mpfp_clears(mpfp_ptr, ...);

/* Assignment */
void mpfp_set_str2(mpfp_ptr, char*);
void mpfp_set_str10(mpfp_ptr, char*);
void mpfp_set(mpfp_ptr, const mpfp_ptr);
void mpfp_set_z(mpfp_ptr, const mpz_ptr);

/* Output */
void mpfp_out_str2(FILE*, const mpfp_ptr);
void mpfp_out_str10(FILE*, const mpfp_ptr);

/* multiplication / division, with rounding */
void mpfp_mul(mpfp_ptr, const mpfp_ptr, const mpfp_ptr);
void mpfp_div(mpfp_ptr, const mpfp_ptr, const mpfp_ptr);

/* common math functions */
void mpfp_exp(mpfp_ptr, const mpfp_ptr);
void mpfp_log(mpfp_ptr, const mpfp_ptr);
void mpfp_pow(mpfp_ptr, const mpfp_ptr, const mpfp_ptr);
void mpfp_sqrt(mpfp_ptr, const mpfp_ptr);

/* trigonometric functions */
void mpfp_sin(mpfp_ptr, const mpfp_ptr);
void mpfp_cos(mpfp_ptr, const mpfp_ptr);
void mpfp_sincos(mpfp_ptr, mpfp_ptr, const mpfp_ptr);
void mpfp_pi(mpfp_ptr);

/* extend or reduce precision, with rounding */
void mpfp_round(mpfp_ptr, const mpfp_ptr);

#endif /* _MPFP_H_ */
