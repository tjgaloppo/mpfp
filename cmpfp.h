/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef _CMPFP_H_
#define _CMPFP_H_

#include "mpfp.h"

typedef struct _cmpfp_t {
	mpfp_t re;
	mpfp_t im;	
} cmpfp_t[1], *cmpfp_ptr;

#define real(x) ((x)->re)
#define imag(x) ((x)->im)

void cmpfp_initialize(int);
void cmpfp_cleanup();

void cmpfp_init(cmpfp_ptr, int);
void cmpfp_clear(cmpfp_ptr);

void cmpfp_set(cmpfp_ptr, const cmpfp_ptr);
void cmpfp_set_fp(cmpfp_ptr, const mpfp_ptr, const mpfp_ptr);

void cmpfp_add(cmpfp_ptr, const cmpfp_ptr, const cmpfp_ptr);
void cmpfp_sub(cmpfp_ptr, const cmpfp_ptr, const cmpfp_ptr);
void cmpfp_mul(cmpfp_ptr, const cmpfp_ptr, const cmpfp_ptr);

void cmpfp_expi(cmpfp_ptr, const mpfp_ptr);

#endif /* _CMPFP_H_ */