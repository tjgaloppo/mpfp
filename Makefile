#
# Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
#
# Permission to use, copy, modify, and/or distribute this software for any 
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
# RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
# CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
CC=gcc
COPT=-O2
RM=rm

all:	mpfp.a cmpfp.a

mpfp.a: mpfp.h mpfp.c
	$(CC) $(COPT) -c -o mpfp.a mpfp.c

cmpfp.a: mpfp.a cmpfp.h cmpfp.c
	$(CC) $(COPT) -c -o cmpfp.a cmpfp.c

test: mpfp.a test.c
	$(CC) $(COPT) -o test test.c mpfp.a -lgmp
	./test	
	
clean:
	$(RM) -f mpfp.a cmpfp.a test
