/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "cmpfp.h"

void cmpfp_init(cmpfp_ptr x, int nbits) {
	mpfp_inits(nbits, real(x), imag(x), NULL);
}

void cmpfp_clear(cmpfp_ptr x) {
	mpfp_clears(real(x), imag(x), NULL);
}

void cmpfp_set(cmpfp_ptr rop, const cmpfp_ptr x) {
	mpfp_set(real(rop), real(x));
	mpfp_set(imag(rop), imag(x));
}

void cmpfp_set_fp(cmpfp_ptr rop, const mpfp_ptr re, const mpfp_ptr im) {
	mpfp_set(real(rop), re);
	mpfp_set(imag(rop), im);	
}

void cmpfp_add(cmpfp_ptr rop, const cmpfp_ptr a, const cmpfp_ptr b) {
	mpfp_add(real(rop), real(a), real(b));
	mpfp_add(imag(rop), imag(a), imag(b));	
}

void cmpfp_sub(cmpfp_ptr rop, const cmpfp_ptr a, const cmpfp_ptr b) {
	mpfp_sub(real(rop), real(a), real(b));
	mpfp_sub(imag(rop), imag(a), imag(b));	
}

static mpfp_t k1, k2, k3;

void cmpfp_initialize(int nbits) {
	mpfp_inits(nbits, k1, k2, k3, NULL);	
}

void cmpfp_cleanup() {
	mpfp_clears(k1, k2, k3, NULL);	
}

#ifndef _NO_CMPFP_FAST_MUL_
void cmpfp_mul(cmpfp_ptr rop, const cmpfp_ptr a, const cmpfp_ptr b) {
//	mpfp_t k1, k2, k3;
//	mpfp_inits(real(rop)->nbits, k1, k2, k3, NULL);
	mpfp_add(k1, real(b), imag(b));
	mpfp_mul(k1, k1, real(a));
	mpfp_add(k2, real(a), imag(a));
	mpfp_mul(k2, k2, imag(b));
	mpfp_sub(k3, imag(a), real(a));
	mpfp_mul(k3, k3, real(b));
	mpfp_sub(real(rop), k1, k2);
	mpfp_add(imag(rop), k1, k3);
//	mpfp_clears(k1, k2, k3, NULL);	
}
#else
void cmpfp_mul(cmpfp_ptr rop, const cmpfp_ptr a, const cmpfp_ptr b) {
	mpfp_t ac, bd, ad, bc;
	mpfp_inits(real(rop)->nbits, ac, bd, ad, bc, NULL);
	mpfp_mul(ac, real(a), real(b));
	mpfp_mul(ad, real(a), imag(b));
	mpfp_mul(bc, imag(a), real(b));
	mpfp_mul(bd, imag(a), imag(b));
	mpfp_sub(real(rop), ac, bd);
	mpfp_add(imag(rop), ad, bc);
	mpfp_clears(bc, ad, bd, ac, NULL);
}
#endif

void cmpfp_expi(cmpfp_ptr rop, const mpfp_ptr x) {
	mpfp_sincos(imag(rop), real(rop), x);	
}
