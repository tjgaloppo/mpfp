/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdio.h>
#include "mpfp.h"

// 50 digits of E, PI, SQRT(2) (accurate to about 166 bits)
#define E_STR      "2.71828182845904523536028747135266249775724709369995"
#define PI_STR     "3.14159265358979323846264338327950288419716939937510"
#define SQRT_2_STR "1.41421356237309504880168872420969807856967187537695"

int check_precision(mpfp_ptr x, char* test_val, int precision) {
	int fail = 0;
	mpfp_t y, z;
	mpfp_inits(2*precision, y, z, NULL);	
	// get check value to twice desired precision
	mpfp_set_str10(z, test_val);
	// extend test value precision 
	mpfp_round(y, x);    
	// get difference between check value and test value        
	mpfp_sub(z, z, y); 					 
	// shift difference down to test precision
	mpz_tdiv_q_2exp(z->value, z->value, precision); 
	z->nbits -= precision;
	// compute precision vale
	mpz_set_ui(y->value, 1); 
	y->nbits = precision;
	// if error is greater than precision value, then test fails
	fail = mpz_cmp(z->value, y->value) > 0;
	if (fail) {
		printf("precision check failure:\n");
		printf("  epsilon = "); mpfp_out_str10(NULL, z); printf("\n");
		printf("      tol = "); mpfp_out_str10(NULL, y); printf("\n");	
	}
	mpfp_clears(z, NULL);
	return fail;
}

int test_add() {
	int fail = 0;
	mpfp_t a, b, c, d;
	
	mpfp_inits(2, a, b, c, d, NULL);
	
	// positive + positive test
	mpfp_set_str10(a, "1.25");
	mpfp_set_str10(b, "2.5");
	mpfp_set_str10(c, "3.75");
	mpfp_add(d, a, b);
	fail = mpfp_cmp(c,d) != 0;
	
	// negative + negative test
	mpfp_set_str10(a, "-1.25");
	mpfp_set_str10(b, "-2.75");
	mpfp_set_str10(c, "-4.0");
	mpfp_add(d, a, b);
	fail |= mpfp_cmp(c,d) != 0;	
	
	// positive + negative test
	mpfp_set_str10(a, "1.5");
	mpfp_set_str10(b, "-2.75");
	mpfp_set_str10(c, "-1.25");
	mpfp_add(d, a, b);
	fail |= mpfp_cmp(c,d) != 0;		
	
	mpfp_clears(a, b, c, NULL);
	
	return fail;	
}

int test_sub() {
	int fail = 0;
	mpfp_t a, b, c, d;
	
	mpfp_inits(3, a, b, c, d, NULL);
	
	// positive - positive test
	mpfp_set_str10(a, "1.25");
	mpfp_set_str10(b, "2.5");
	mpfp_set_str10(c, "-1.25");
	mpfp_sub(d, a, b);
	fail = mpfp_cmp(c,d) != 0;
	
	// negative - negative test
	mpfp_set_str10(a, "-1.25");
	mpfp_set_str10(b, "-2.75");
	mpfp_set_str10(c, "1.5");
	mpfp_sub(d, a, b);
	fail |= mpfp_cmp(c,d) != 0;	
	
	// positive - negative test
	mpfp_set_str10(a, "1.5");
	mpfp_set_str10(b, "-2.75");
	mpfp_set_str10(c, "4.25");
	mpfp_sub(d, a, b);
	fail |= mpfp_cmp(c,d) != 0;		

	// negative - positive test
	mpfp_set_str10(a, "-1.5");
	mpfp_set_str10(b, "2.125");
	mpfp_set_str10(c, "-3.625");
	mpfp_sub(d, a, b);
	fail |= mpfp_cmp(c,d) != 0;
	
	mpfp_clears(a, b, c, NULL);
	
	return fail;	
}

int test_mul() {
	int fail = 0;
	mpfp_t a, b, c, d;
	mpfp_inits(2, a, b, c, d, NULL);

	// exact test
	mpfp_set_str2(a, "1.10");
	mpfp_set_str2(b, "0.10");
	mpfp_set_str2(c, "0.11");
	mpfp_mul(d, a, b);
	fail = mpfp_cmp(c, d) != 0;

	// test with rounding
	mpfp_set_str2(a, "1.10");
	mpfp_set_str2(b, "0.01");
	mpfp_set_str2(c, "0.10");
	mpfp_mul(d, a, b);
	fail |= mpfp_cmp(c, d) != 0;

	mpfp_clears(a, b, c, d, NULL);
	return fail;
}

int test_div() {
	int fail = 0;
	mpfp_t a, b, c, d;
	mpfp_inits(2, a, b, c, d, NULL);
	
	// exact test 1
	mpfp_set_str10(a, "0.5");
	mpfp_set_str10(b, "0.25");
	mpfp_set_str10(c, "2.0");
	mpfp_div(d, a, b);
	fail = mpfp_cmp(c, d) != 0;
	
	// exact test 2
	mpfp_set_str10(a, "1.0");
	mpfp_set_str10(b, "2.0");
	mpfp_set_str10(c, "0.5");
	mpfp_div(d, a, b);
	fail |= mpfp_cmp(c, d) != 0;	
	
	// test with rounding
	mpfp_set_str2(a, "1.01");
	mpfp_set_str2(b, "0.11");
	mpfp_set_str2(c, "1.11");
	mpfp_div(d, a, b);
	fail |= mpfp_cmp(c, d) != 0;	
	
	mpfp_clears(a, b, c, d, NULL);	
	return fail;
}

int test_abs() {
	int fail = 0;
	mpfp_t a, b, c;
	mpfp_inits(2, a, b, c, NULL);

	mpfp_set_str10(a, "-10.0");
	mpfp_set_str10(b, "10.0");
	mpfp_abs(c, a);
	fail = mpfp_cmp(b, c) != 0;

	mpfp_set_str10(a, "5.0");
	mpfp_set_str10(b, "5.0");
	mpfp_abs(c, a);
	fail |= mpfp_cmp(b, c) != 0;
	
	mpfp_clears(a, b, c, NULL);
	return fail;
}

int test_neg() {
	int fail = 0;
	mpfp_t a, b, c;
	mpfp_inits(2, a, b, c, NULL);

	mpfp_set_str10(a, "-10.0");
	mpfp_set_str10(b, "10.0");
	mpfp_neg(c, a);
	fail = mpfp_cmp(b, c) != 0;

	mpfp_set_str10(a, "5.0");
	mpfp_set_str10(b, "-5.0");
	mpfp_neg(c, a);
	fail |= mpfp_cmp(b, c) != 0;	
	
	return fail;
}

int test_sgn() {
	int fail = 0;
	mpfp_t a;
	mpfp_init(a, 2);
	mpfp_set_str10(a, "1.0");
	fail = mpfp_sgn(a) <= 0;
	mpfp_set_str10(a, "-1.0");
	fail |= mpfp_sgn(a) >= 0;
	mpfp_clear(a);
	return fail;
}

int test_exp() {
	int fail = 0;
	
	mpfp_t a, b;
	
	for (int j=2; j<165; j++) {
		mpfp_inits(j, a, b, NULL);

		// test exp(0)
		mpfp_set_str10(a, "0");
		mpfp_exp(b, a);
		fail |= check_precision(b, "1.0", j) != 0;
		
		// test exp(1) to precision
		mpfp_set_str10(a, "1");
		mpfp_exp(b, a);		
		fail |= check_precision(b, E_STR, j) != 0;
	
		mpfp_clears(a, b, NULL);
	}
	
	return fail;
}

int test_log() {
	int fail = 0;
	
	mpfp_t a, b;
	
	for (int j=2; j<165; j++) {
		mpfp_inits(j, a, b, NULL);
		
		// check log(1)
		mpfp_set_str10(a, "1");
		mpfp_log(b, a);
		fail |= check_precision(b, "0", j) != 0;
		
		// check log(e)
		mpfp_set_str10(a, E_STR);
		mpfp_log(b, a);
		fail |= check_precision(b, "1", j) != 0;
		
		mpfp_clears(a, b, NULL);	
	}
	
	return fail;
}

int test_pow() {
	int fail = 0;
	
	mpfp_t a, b, c;
	
	for (int j=6; j<30; j++) {
		mpfp_inits(j, a, b, c, NULL);
		
		mpfp_set_str10(a, "2.0");
		mpfp_set_str10(b, "1.5");
		mpfp_pow(c, a, b);
		fail |= check_precision(c, "2.828427124746190", j) != 0;
		
		mpfp_clears(a, b, c, NULL);	
	}	
	
	return fail;
}

int test_sqrt() {
	int fail = 0;
	mpfp_t a, b;
	
	for (int j=3; j<100; j++) {
		mpfp_inits(j+1, a, b, NULL);
		
		mpfp_set_str10(a, "25");
		mpfp_sqrt(b, a);
		fail |= check_precision(b, "5", j) != 0;
		
		mpfp_set_str10(a, "0.25");
		mpfp_sqrt(b, a);
		fail |= check_precision(b, "0.5", j) != 0;		
		
		mpfp_set_str10(a, "2");
		mpfp_sqrt(b, a);
		fail |= check_precision(b, SQRT_2_STR, j) != 0;
		
		mpfp_clears(a, b, NULL);	
	}
	
	return fail;
}

int test_sin() {
	int fail = 0;
	
	mpfp_t pi, fp30, fp45, fp60, fp90, fp180, fp270, deg2rad, x;
	
	for (int j=20; j<45; j++) {
		mpfp_inits(j+6, pi, fp30, fp45, fp60, fp90, fp180, fp270, deg2rad, x, NULL);

		mpfp_pi(pi);		
		mpfp_set_str10(fp30, "30");
		mpfp_set_str10(fp45, "45");
		mpfp_set_str10(fp60, "60");
		mpfp_set_str10(fp90, "90");
		mpfp_set_str10(fp180, "180");
		mpfp_set_str10(fp270, "270");
		
		mpfp_div(deg2rad, pi, fp180);
		
		mpfp_mul(x, fp30, deg2rad);
		mpfp_sin(x, x);
		fail |= check_precision(x, "0.5", j) != 0;
		
		mpfp_mul(x, fp45, deg2rad);
		mpfp_sin(x, x);
		fail |= check_precision(x, "0.70710678118655", j) != 0;
		
		mpfp_mul(x, fp60, deg2rad);
		mpfp_sin(x, x);
		fail |= check_precision(x, "0.86602540378444", j) != 0;
		
		mpfp_mul(x, fp90, deg2rad);
		mpfp_sin(x, x);
		fail |= check_precision(x, "1", j) != 0;
		
		mpfp_mul(x, fp180, deg2rad);
		mpfp_sin(x, x);
		fail |= check_precision(x, "0", j) != 0;
		
		mpfp_mul(x, fp270, deg2rad);
		mpfp_sin(x, x);
		fail |= check_precision(x, "-1", j) != 0;										
		
		mpfp_clears(x, deg2rad, fp270, fp180, fp90, fp60, fp45, fp30, pi, NULL);	
	}
	
	return fail;
}

int test_cos() {
	int fail = 0;
	
	mpfp_t pi, fp30, fp45, fp60, fp90, fp180, fp270, deg2rad, x;
	
	for (int j=20; j<45; j++) {
		mpfp_inits(j+6, pi, fp30, fp45, fp60, fp90, fp180, fp270, deg2rad, x, NULL);

		mpfp_pi(pi);		
		mpfp_set_str10(fp30, "30");
		mpfp_set_str10(fp45, "45");
		mpfp_set_str10(fp60, "60");
		mpfp_set_str10(fp90, "90");
		mpfp_set_str10(fp180, "180");
		mpfp_set_str10(fp270, "270");
		
		mpfp_div(deg2rad, pi, fp180);
		
		mpfp_mul(x, fp30, deg2rad);
		mpfp_cos(x, x);
		fail |= check_precision(x, "0.86602540378444", j) != 0;
		
		mpfp_mul(x, fp45, deg2rad);
		mpfp_cos(x, x);
		fail |= check_precision(x, "0.70710678118655", j) != 0;
		
		mpfp_mul(x, fp60, deg2rad);
		mpfp_cos(x, x);
		fail |= check_precision(x, "0.5", j) != 0;
		
		mpfp_mul(x, fp90, deg2rad);
		mpfp_cos(x, x);
		fail |= check_precision(x, "0", j) != 0;
		
		mpfp_mul(x, fp180, deg2rad);
		mpfp_cos(x, x);
		fail |= check_precision(x, "-1", j) != 0;
				
		mpfp_mul(x, fp270, deg2rad);
		mpfp_cos(x, x);
		fail |= check_precision(x, "0", j) != 0;										
		
		mpfp_clears(x, deg2rad, fp270, fp180, fp90, fp60, fp45, fp30, pi, NULL);	
	}	
	
	return fail;
}

int test_sincos() {
	int fail = 0;
	
	mpfp_t pi, fp30, fp45, fp60, fp90, fp180, fp270, deg2rad, x, s, c;
	
	for (int j=20; j<45; j++) {
		mpfp_inits(j+6, pi, fp30, fp45, fp60, fp90, fp180, fp270, deg2rad, x, s, c, NULL);

		mpfp_pi(pi);		
		mpfp_set_str10(fp30, "30");
		mpfp_set_str10(fp45, "45");
		mpfp_set_str10(fp60, "60");
		mpfp_set_str10(fp90, "90");
		mpfp_set_str10(fp180, "180");
		mpfp_set_str10(fp270, "270");
		
		mpfp_div(deg2rad, pi, fp180);
		
		mpfp_mul(x, fp30, deg2rad);
		mpfp_sincos(s, c, x);
		fail |= check_precision(s, "0.5", j) != 0;
		fail |= check_precision(c, "0.86602540378444", j) != 0;
		
		mpfp_mul(x, fp45, deg2rad);
		mpfp_sincos(s, c, x);
		fail |= check_precision(s, "0.70710678118655", j) != 0;
		fail |= check_precision(c, "0.70710678118655", j) != 0;
		
		mpfp_mul(x, fp60, deg2rad);
		mpfp_sincos(s, c, x);
		fail |= check_precision(s, "0.86602540378444", j) != 0;
		fail |= check_precision(c, "0.5", j) != 0;
		
		mpfp_mul(x, fp90, deg2rad);
		mpfp_sincos(s, c, x);
		fail |= check_precision(s, "1", j) != 0;
		fail |= check_precision(c, "0", j) != 0;
		
		mpfp_mul(x, fp180, deg2rad);
		mpfp_sincos(s, c, x);
		fail |= check_precision(s, "0", j) != 0;
		fail |= check_precision(c, "-1", j) != 0;
		
		mpfp_mul(x, fp270, deg2rad);
		mpfp_sincos(s, c, x);
		fail |= check_precision(s, "-1", j) != 0;	
		fail |= check_precision(c, "0", j) != 0;									
		
		mpfp_clears(c, s, x, deg2rad, fp270, fp180, fp90, fp60, fp45, fp30, pi, NULL);	
	}	
	
	return fail;
}

int test_pi() {
	int fail = 0;
	
	mpfp_t pi;
	
	for (int j=3; j<165; j++) {
		mpfp_init(pi, j);
		mpfp_pi(pi);
		fail |= check_precision(pi, PI_STR, j);
		mpfp_clear(pi);
	}
	
	return fail;
}

typedef int (*test_func)();
typedef struct {
	char* name;
	test_func f;
} test_t;

test_t funcs[] = {
	{"ADD", test_add}, {"SUB", test_sub}, 
	{"MUL", test_mul}, {"DIV", test_div},
	{"ABS", test_abs}, {"NEG", test_neg}, 
	{"SGN", test_sgn}, {"EXP", test_exp}, 
	{"LOG", test_log}, {"POW", test_pow}, 
	{"PI", test_pi},   {"SQRT", test_sqrt}, 
	{"COS", test_cos}, {"SIN", test_sin}, 
	{"SINCOS", test_sincos} 
};
int nfuncs = sizeof(funcs) / sizeof(test_t);

int main(int argc, char** argv) {	
	int failed = 0;
	for (int j=0; j<nfuncs; j++) {
		printf("TESTING '%s': ", funcs[j].name); fflush(stdout);
		if (funcs[j].f() != 0) {
			printf("FAILED\n");
			failed += 1;	
		} else {
			printf("PASSED\n");
		}
	}
	if (failed) {
		printf("WARNING: %d tested FAILED\n", failed);
	}
	return failed;
}
