/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "mpfp.h"

void mpfp_init(mpfp_ptr x, int nbits) {
	mpz_init2(x->value, 2*nbits+1);
	x->nbits = nbits;	
}

void mpfp_inits(int nbits, ...) {
	va_list vargs;
	va_start(vargs, nbits);
	mpfp_ptr x = va_arg(vargs, mpfp_ptr);
	while (NULL != x) {
		mpfp_init(x, nbits);
		x = va_arg(vargs, mpfp_ptr);	
	}
	va_end(vargs);	
}

void mpfp_clears(mpfp_ptr x, ...) {
	va_list vargs;
	va_start(vargs, x);
	while (NULL != x) {
		mpfp_clear(x);
		x = va_arg(vargs, mpfp_ptr);	
	}
	va_end(vargs);	
}

void mpfp_mul(mpfp_ptr rop, const mpfp_ptr a, const mpfp_ptr b) {
	mpz_mul(rop->value, a->value, b->value);
	mpz_tdiv_q_2exp(rop->value, rop->value, rop->nbits-1);
	if (mpz_sgn(rop->value) >= 0)
		mpz_add_ui(rop->value, rop->value, 1);
	else 
		mpz_sub_ui(rop->value, rop->value, 1);
	mpz_tdiv_q_2exp(rop->value, rop->value, 1);
}

void mpfp_div(mpfp_ptr rop, const mpfp_ptr a, const mpfp_ptr b) {
	mpz_mul_2exp(rop->value, a->value, rop->nbits+1);
	mpz_tdiv_q(rop->value, rop->value, b->value);
	if (mpz_sgn(rop->value) >= 0) 
		mpz_add_ui(rop->value, rop->value, 1);
	else
		mpz_sub_ui(rop->value, rop->value, 1);
	mpz_tdiv_q_2exp(rop->value, rop->value, 1);
}

// exp() using Taylor series until term vanishes
void mpfp_exp(mpfp_ptr rop, const mpfp_ptr x) {
	mpfp_t X, one, sum, term, xpow, denom, counter;
	mpfp_inits(rop->nbits + 1, X, one, sum, term, xpow, denom, counter, NULL);
	mpfp_round(X, x);
	mpfp_set_str2(one, "1");
	mpfp_set(sum, one);
	mpfp_set(xpow, one);
	mpfp_set(denom, one);
	mpfp_set(counter, one);
	do {
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(denom, denom, counter);
		mpfp_div(term, xpow, denom);
		mpfp_add(sum, sum, term);
		mpfp_add(counter, counter, one);
	} while(mpz_cmp_si(term->value, 0) != 0);
	mpfp_round(rop, sum);
	mpfp_clears(counter, denom, xpow, term, sum, one, X, NULL);
}

// iterative y_(n+1) = y_n + 2 * (x - exp(y_n)) / (x + exp(y_n))
void mpfp_log(mpfp_ptr rop, const mpfp_ptr x) {
	mpfp_t X, y, term, pterm, num, den, expy;
	mpfp_inits(rop->nbits+1, X, y, term, pterm, num, den, expy, NULL);
	mpfp_round(X, x);
	mpfp_set_str2(y,"0");
	mpfp_set_str2(term, "0");
	do {
		mpfp_set(pterm, term);
		mpfp_exp(expy, y);
		mpfp_sub(num, X, expy);
		mpfp_add(den, X, expy);
		mpfp_div(term, num, den);
		mpz_mul_2exp(term->value, term->value, 1);
		mpfp_add(y, y, term);
	} while (mpz_cmpabs(term->value, pterm->value) != 0);
	mpfp_round(rop, y);
	mpfp_clears(expy, den, num, pterm, term, y, X, NULL);
}

// x^a = exp(a * log(x))
void mpfp_pow(mpfp_ptr rop, const mpfp_ptr x, const mpfp_ptr a) {
	mpfp_t X, A, u;
	mpfp_inits(rop->nbits + 1, X, A, u, NULL);
	mpfp_round(X, x);
	mpfp_round(A, a);
	mpfp_log(u, X);
	mpfp_mul(u, u, A);
	mpfp_exp(u, u);
	mpfp_round(rop, u);
	mpfp_clears(u, A, X, NULL);
}

// sqrt(x) = exp(0.5 * log(x))
void mpfp_sqrt(mpfp_ptr rop, const mpfp_ptr x) {
	mpfp_t half;
	mpfp_init(half, rop->nbits);
	mpfp_set_str10(half, "0.5");
	mpfp_pow(rop, x, half);
	mpfp_clear(half);
}

// cosine by Taylor series
void mpfp_cos(mpfp_ptr rop, const mpfp_ptr x) {
	mpfp_t X, xpow, fact, term, sum, one, count, pm;
	mpfp_inits(rop->nbits+1, X, xpow, fact, term, sum, one, count, pm, NULL);
	mpfp_round(X, x);
	mpfp_set_str2(one, "1");
	mpfp_set(sum, one);
	mpfp_set(xpow, X);
	mpfp_set(fact, one);
	mpfp_set(count, one);
	mpfp_set(pm, one);
	do {
		mpfp_neg(pm, pm);
		mpfp_add(count, count, one);
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(fact, fact, count);
		mpfp_div(term, xpow, fact);
		mpfp_mul(term, term, pm);
		mpfp_add(sum, sum, term); 
		mpfp_add(count, count, one);
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(fact, fact, count);
	} while (mpz_cmp_si(term->value, 0) != 0);
	mpfp_round(rop, sum);
	mpfp_clears(pm, count, one, sum, term, fact, xpow, X, NULL);
}

// sine by Taylor series
void mpfp_sin(mpfp_ptr rop, const mpfp_ptr x) {
	mpfp_t X, xpow, fact, term, sum, one, count, pm;
	mpfp_inits(rop->nbits+1, X, xpow, fact, term, sum, one, count, pm, NULL);
	mpfp_round(X, x);
	mpfp_set_str2(one, "1");
	mpfp_set(sum, X);
	mpfp_set(xpow, X);
	mpfp_set(fact, one);
	mpfp_set(count, one);
	mpfp_set(pm, one);
	do {
		mpfp_neg(pm, pm);
		mpfp_add(count, count, one);
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(fact, fact, count);
		mpfp_add(count, count, one);
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(fact, fact, count);
		mpfp_div(term, xpow, fact);
		mpfp_mul(term, term, pm);
		mpfp_add(sum, sum, term);				
	} while (mpz_cmp_si(term->value, 0) != 0);
	mpfp_round(rop, sum);
	mpfp_clears(pm, count, one, sum, term, fact, xpow, X, NULL);
}

// combined sin/cos from Taylor series
void mpfp_sincos(mpfp_ptr sinop, mpfp_ptr cosop, const mpfp_ptr x) {
	mpfp_t X, xpow, fact, term, ssum, csum, one, count, pm;
	mpfp_inits(sinop->nbits+1, X, xpow, fact, term, ssum, csum, one, count, pm, NULL);
	mpfp_round(X, x);
	mpfp_set_str2(one, "1");
	mpfp_set(ssum, X);
	mpfp_set(csum, one);
	mpfp_mul(xpow, X, one);
	mpfp_set(fact, one);
	mpfp_set(count, one);
	mpfp_set(pm, one);
	do {
		mpfp_neg(pm, pm);
		mpfp_add(count, count, one);
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(fact, fact, count);
		mpfp_div(term, xpow, fact);
		mpfp_mul(term, term, pm);
		mpfp_add(csum, csum, term); // end cosine part
		mpfp_add(count, count, one);
		mpfp_mul(xpow, xpow, X);
		mpfp_mul(fact, fact, count);
		mpfp_div(term, xpow, fact);
		mpfp_mul(term, term, pm);
		mpfp_add(ssum, ssum, term);	// end sine part	
	} while (mpz_cmp_si(term->value, 0) != 0);
	mpfp_round(sinop, ssum);
	mpfp_round(cosop, csum);
	mpfp_clears(pm, count, one, csum, ssum, term, fact, xpow, X, NULL);
}

// pi via Gauss-Legendre algorithm 
// (https://en.wikipedia.org/wiki/Gauss%E2%80%93Legendre_algorithm)
void mpfp_pi(mpfp_ptr rop) {
	int padbits = 8;
	mpfp_t an, bn, tn, pn, an1, temp, prev, two;
	mpfp_inits(rop->nbits+padbits, an, bn, tn, pn, an1, temp, prev, two, NULL);
	mpfp_set_str2(an, "1");
	mpfp_set_str10(tn, "0.25");
	mpfp_set_str2(pn, "1");
	mpfp_set_str2(bn, "1");
	mpfp_set_str10(temp, "2");
	mpfp_set_str10(two, "2");
	mpfp_sqrt(temp, temp);
	mpfp_div(bn, bn, temp);
	do {
		mpfp_set(prev, temp);
		mpfp_add(an1, an, bn);		
		mpfp_div(an1, an1, two);
		mpfp_mul(temp, an, bn);
		mpfp_sqrt(bn, temp);
		mpfp_sub(temp, an, an1);
		mpfp_mul(temp, temp, temp);
		mpfp_mul(temp, temp, pn);
		mpfp_sub(tn, tn, temp);
		mpz_mul_2exp(pn->value, pn->value, 1);
		mpfp_set(an, an1);
		mpfp_sub(temp, an, bn);
	} while(mpz_cmp_si(temp->value, 0) != 0 && mpz_cmp(temp->value, prev->value) != 0);
	mpfp_add(temp, an, bn);
	mpfp_mul(temp, temp, temp);
	mpz_mul_2exp(tn->value, tn->value, 2); 
	mpfp_div(temp, temp, tn);
	mpfp_round(rop, temp);
	mpfp_clears(two, prev, temp, an1, pn, tn, bn, an, NULL);
}

void mpfp_round(mpfp_ptr rop, const mpfp_ptr x) {
	mpz_t round;
	int deltabits = x->nbits - rop->nbits;
	if (deltabits <= 0) {
		mpz_mul_2exp(rop->value, x->value, -deltabits);
	} else {
		mpz_init(round);
		mpz_ui_pow_ui(round, 2, deltabits - 1);
		mpz_set(rop->value, x->value);
		if (mpz_sgn(rop->value) >= 0)
			mpz_add(rop->value, rop->value, round);
		else
			mpz_sub(rop->value, rop->value, round);
		mpz_tdiv_q_2exp(rop->value, rop->value, deltabits);
		mpz_clear(round);
	}
}

void mpfp_set(mpfp_ptr rop, const mpfp_ptr x) {
	mpz_set(rop->value, x->value);
	rop->nbits = x->nbits;	
}

void mpfp_set_z(mpfp_ptr rop, const mpz_ptr x) {
	mpz_mul_2exp(rop->value, x, rop->nbits);	
}

void mpfp_set_str2(mpfp_ptr x, char* str2) {
	int len = strlen(str2);
	int neg = str2[0] == '-';
	int p0 = neg ? 1 : 0;
	int dec = 0;
	
	mpz_set_ui(x->value, 0);
	
	for (int j=p0; j<len; j++) {
		if (str2[j] == '.') {
			dec = len - j - 1;
		} else {
			mpz_mul_ui(x->value, x->value, 2);
			mpz_add_ui(x->value, x->value, str2[j] - '0');	
		}
	}	
	
	if (dec < x->nbits) 
		mpz_mul_2exp(x->value, x->value, x->nbits - dec);

	if (neg) 
		mpz_neg(x->value, x->value);	
}

void mpfp_set_str10(mpfp_ptr rop, char* str10) {
	int len = strlen(str10);
	int neg = str10[0] == '-';
	int p0 = neg ? 1 : 0;
		
	mpfp_t x;
	mpfp_init(x, rop->nbits + 1);
	
	mpz_set_ui(x->value, 0);

	mpz_t int_part, value;
	mpz_inits(int_part, value, NULL);
	mpz_set_si(int_part, 0);
	
	mpfp_t frac_part, ten;
	mpfp_inits(x->nbits, frac_part, ten, NULL);
	mpfp_set_str2(frac_part, "0");
	mpfp_set_str2(ten, "1010");
	
	int frac = 0;
	for (int j=p0; j<len; j++) {
		if (str10[j] == '.') {
			frac = j;
		} else if (frac) {
			mpz_set_ui(value, str10[len-(j-frac)] - '0');
			mpz_mul_2exp(value, value, x->nbits);
			mpz_add(frac_part->value, frac_part->value, value);
			mpfp_div(frac_part, frac_part, ten);
		} else {
			mpz_mul_si(int_part, int_part, 10);
			mpz_add_ui(int_part, int_part, str10[j] - '0');	
		}	
	}
	
	mpz_mul_2exp(x->value, int_part, x->nbits);
	mpz_add(x->value, x->value, frac_part->value);
	mpz_add_ui(x->value, x->value, 1);
	mpz_tdiv_q_2exp(rop->value, x->value, 1);
	
	if (neg) 
		mpz_neg(rop->value, rop->value);	
	
	mpfp_clear(x);	
}

void mpfp_out_str2(FILE* file, const mpfp_ptr x) {
	if (NULL == file)
		file = stdout;

	int sign = mpz_sgn(x->value);
	
	mpz_t value;
	mpz_init(value);
	mpz_abs(value, x->value);
	
	int n = mpz_sizeinbase(value, 2);
	if (n < x->nbits + 1) n = x->nbits + 1;
	
	if(sign < 0) 
		fprintf(file, "-");	

	for (int j=0; j<n; j++) {
		if (n-j == x->nbits) fprintf(file, ".");
		fprintf(file, "%d", mpz_tstbit(value, n-j-1));	
	}

	mpz_clear(value);	
}

void mpfp_out_str10(FILE* file, const mpfp_ptr x) {
	if (NULL == file)
		file = stdout;	
		
	int sign = mpz_sgn(x->value);
	
	mpz_t value, int_part, frac_part;
	mpz_inits(value, int_part, frac_part, NULL);
	
	mpz_abs(value, x->value);	
	mpz_tdiv_q_2exp(int_part, value, x->nbits);
	mpz_tdiv_r_2exp(frac_part, value, x->nbits);
	
	if (sign < 0) 
		fprintf(file, "-");
	mpz_out_str(file, 10, int_part);
	if (mpz_cmp_si(frac_part, 0) > 0) {
		fprintf(file, ".");
		while (mpz_cmp_si(frac_part, 0) > 0) {
			mpz_mul_si(value, frac_part, 10);
			mpz_fdiv_q_2exp(frac_part, value, x->nbits);
			mpz_out_str(file, 10, frac_part);
			mpz_fdiv_r_2exp(frac_part, value, x->nbits);
		}
	}

	mpz_clears(frac_part, int_part, value, NULL);
}
