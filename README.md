# ABOUT #

This code implements a set of functions for arbitrary precision *fixed-point* arithmetic (both real and complex).  It is built on top of the fantastic GNU MP library (https://gmplib.org/).

I needed this code for some research I was working on, and could not find a suitable fixed-point library available.  It is poorly documented, but I make it available here in case anyone else is looking for a fixed-point arithmetric library.

# FEATURES #

The library supports the following operations for fixed-point real numbers:

* add, subtract, multiply, divide
* absolute value, negation, sign
* comparison
* logarithm, powers, exponentiation (e^x), sqrt
* sine, cosine, pi
* rounding

Limited support for complex values is also provided, including:

* add, subtract, multiply
* exponentiation e^(ix)

# BUILDING #

The GNU MP library is required for building.  The existing Makefile assumes that GNU MP is available without additional library paths. Assuming this is correct, you can build and test the code via:

```
make
make test
```

# LICENSE #

This code is provided under the [very permissive] ISC license (https://opensource.org/licenses/ISC).
(Primarily to grant explicit permission to use this code).  Please keep in mind that this license differs from that of the GNU MP library.

# AUTHOR / MAINTAINER #

Travis Galoppo (tjg2107 AT columbia.edu)